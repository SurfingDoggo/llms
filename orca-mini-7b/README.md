# Orca Mini LLM
This takes Orca Mini 7b:

https://huggingface.co/psmathur/orca_mini_7b

and containerizes it. Currently only tested locally in WSL+nvidia 3060ti mobile.

Like+subscribe+comment, more to come!

Coming soon:
- [ ] Microservicified
- [ ] Kubernetification with Helm
- [ ] Logging+monitoring
- [ ] Cloudification
- [ ] CI/CD+container image repository
- [ ] ???
- [ ] `$$$$$$$$`

## initial setup
The model is about 30gb. To save it locally:
* change `local_path` for `model_path` everywhere but where the variables are set
* uncomment the two `.save_pretrained` lines to save the model locally

Once it's downloaded, switch it all back.

## Working with Docker image
### build
`docker build -t orca . --progress=plain`

`--no-cache` can be helpful, it builds from scratch (Docker caches whatever layers of the image it can).
### run 
#### run main.py
`docker run --rm -it --name orca --volume $(pwd)/:/code --gpus all orca:latest python3.9 main.py`
##### WSL
On Windows System Linux, for some reason you need --env NVIDIA_DISABLE_REQUIRE=1

`docker run --rm -it --name orca --volume $(pwd)/:/code --gpus all --env NVIDIA_DISABLE_REQUIRE=1 orca:latest python3.9 main.py`

#### open bash
To open up with a shell:

`docker run --rm -it --name orca --volume $(pwd)/:/code --gpus all --env NVIDIA_DISABLE_REQUIRE=1 orca:latest bash`

and then run `python3.9 main.py`

## monitor
This runs a container with nvidia-smi to monitor the graphics card:

`docker run --rm -it --gpus all --env NVIDIA_DISABLE_REQUIRE=1 nvidia/cuda:11.6.2-base-ubuntu20.04 nvidia-smi -l 10`
