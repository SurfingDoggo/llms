import time
import torch
from transformers import LlamaForCausalLM, LlamaTokenizer

print("IS AVAILABLE", torch.cuda.is_available())

## saves to offload dir:
local_path = './orca_mini_7b/'
## download on demand:
model_path = 'psmathur/orca_mini_7b'

# Hugging Face model
tokenizer = LlamaTokenizer.from_pretrained(local_path)
model = LlamaForCausalLM.from_pretrained(
  local_path, 
  torch_dtype=torch.float16, 
  device_map='cuda',
  resume_download=True,
)

#model.save_pretrained(local_path)
#tokenizer.save_pretrained(local_path)
#generate text function
def generate_text(system, instruction, input=None):
    
    if input:
        prompt = f"### System:\n{system}\n\n### User:\n{instruction}\n\n### Input:\n{input}\n\n### Response:\n"
    else:
        prompt = f"### System:\n{system}\n\n### User:\n{instruction}\n\n### Response:\n"
    
    tokens = tokenizer.encode(prompt)
    tokens = torch.LongTensor(tokens).unsqueeze(0)
    tokens = tokens.to('cuda')

    instance = {'input_ids': tokens,'top_p': 1.0, 'temperature':0.7, 'generate_len': 1024, 'top_k': 50}

    length = len(tokens[0])
    with torch.no_grad():
        rest = model.generate(
            input_ids=tokens, 
            max_length=length+instance['generate_len'], 
            use_cache=True, 
            do_sample=True, 
            top_p=instance['top_p'],
            temperature=instance['temperature'],
            top_k=instance['top_k']
        )    
    output = rest[0][length:]
    string = tokenizer.decode(output, skip_special_tokens=True)
    return f'[!] Response: {string}'

# instructions
system = 'You are an extremely creative rap artist that writes very catchy chart toppers. Spit lyrics as much as you can.'
instruction = 'Write a rap song making instant coffee dissolved in cold milk sound as cool as possible.'
start_time = time.time()
print(generate_text(system, instruction))
end_time = time.time() - start_time
print("execution time:", end_time, "seconds")
